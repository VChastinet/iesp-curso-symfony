<?php
/**
 * Created by PhpStorm.
 * User: lab05usuario21
 * Date: 09/06/2018
 * Time: 11:53
 */

namespace Infrastructure\Persistence\Doctrine;


use Doctrine\ORM\EntityRepository;
use Domain\Model\Inscricao;
use Domain\Repository\InscricaoRepositoryInterface;

class InscricaoRepository extends EntityRepository implements InscricaoRepositoryInterface
{
    /**
     * @param Inscricao $inscricao
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function salvar(Inscricao $inscricao)
    {
        $this->getEntityManager()->persist($inscricao);
        $this->getEntityManager()->flush();
    }

    /**
     * @param Inscricao $inscricao
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     */
    public function findOneByCpf(Inscricao $inscricao)
    {
        $entityManager = $this->getEntityManager();

        $dql = "
          SELECT inscricao
          FROM Domain\Model\Inscricao inscricao
          JOIN inscricao.oportunidade oportunidade
          JOIN inscricao.candidato candidato
          WHERE oportunidade.idOportunidade = :oportunidadeId
          and candidato.cpf = : candidato_cpf
        ";

        $query = $entityManager->createQuery($dql)->setMaxResults(1);
        $query->setParameter('candidato_cpf', $inscricao->getCandidato()->getCpf());
        $query->setParameter('oportunidadeId', $inscricao->getOportunidade()->getIdOportunidade());

        return $query->getOneOrNullResult();
    }
}