<?php

namespace Infrastructure\Service;

use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;

class SerializerService
{
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * SerializerService constructor.
     * @param Serializer $serializer
     */
    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    public function converter($json, $tipo)
    {
//        return $this->serializer->deserialize($json, $tipo, 'json');
        try {
            return $this->serializer->deserialize($json, $tipo, 'json');
        } catch (\Exception $exception) {
            dump($exception->getMessage()); die;
        }
    }

    public function toJsonByGroups ($data, array $groups = ['default']) {
        return $this->serializer->serialize(
            $data,
            'json',
            SerializationContext::create()->setGroups($groups)
        );
    }

}