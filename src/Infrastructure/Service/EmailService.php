<?php /** @noinspection PhpParamsInspection */

/**
 * Created by PhpStorm.
 * User: lab05usuario21
 * Date: 09/06/2018
 * Time: 14:52
 */

namespace Infrastructure\Service;


use Domain\Model\Inscricao;

class EmailService
{
    public function enviarNotificacaoInscricao(Inscricao $inscricao)
    {
        $candidato = $inscricao->getCandidato();
        $destinatario = $candidato->getEmail();
        $nomeDestinatario = $candidato->getNome();
        $assunto = "inscrição - código de confirmação";
        $corpoEmail = "Olá, " . $nomeDestinatario . "\n" .
            "Seu código de confirmação de inscrição é: " . $inscricao->gerarCodigoConfirmacao();

        $this->enviarNotificacao($destinatario, $nomeDestinatario, $assunto, $corpoEmail);
    }

    public function enviarNotificacao(
        string $destinatario,
        string $nomeDestinatario,
        string $assunto,
        string $corpoEmail
    ) {
        $transport = new \Swift_SmtpTransport('mail.smtp2go.com', 2525);
        $transport->setUsername('victorxst@gmail.com');
        $transport->setPassword('victorcursosymfony');

        $mailer = new \Swift_Mailer($transport);

        $message = new \Swift_Message($assunto);

        $message->setFrom(["suport.inscricao@hotmail.com" => "suporte - Inscricao"]);
        $message->setTo([$destinatario => $nomeDestinatario]);
        $message->setBody($corpoEmail);

        $mailer->send($message);
    }
}