<?php
/**
 * Created by PhpStorm.
 * User: vchastinet
 * Date: 06/06/18
 * Time: 22:08
 */

namespace Domain\Model;


use Doctrine\Common\Collections\Collection;

class Candidato
{
    /**
     * @var int
     */
    private $idCandidato;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $cpf;

    /**
     * @var string
     */
    private $nome;

    /**
     * @var string
     */
    private $telefone;

    /**
     * @var string
     */
    private $curriculo;


    /**
     * @var Collection
     */
    private $habilidadesTecnicas;

    /**
     * @var Collection
     */
    private $experienciasProfissionais;

    /**
     * @return int
     */
    public function getIdCandidato()
    {
        return $this->idCandidato;
    }

    /**
     * @return string
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @return string
     */
    public function getTelefone()
    {
        return $this->telefone;
    }

    /**
     * @return string
     */
    public function getCurriculo()
    {
        return $this->curriculo;
    }

    /**
     * @return Collection
     */
    public function getHabilidadesTecnicas()
    {
        return $this->habilidadesTecnicas;
    }

    /**
     * @return Collection
     */
    public function getExperienciasProfissionais()
    {
        return $this->experienciasProfissionais;
    }

}