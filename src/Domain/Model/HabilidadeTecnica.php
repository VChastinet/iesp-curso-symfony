<?php
/**
 * Created by PhpStorm.
 * User: vchastinet
 * Date: 06/06/18
 * Time: 22:10
 */

namespace Domain\Model;


class HabilidadeTecnica
{
    /**
     * @var int
     */
    private $idHabilidadeTecnica;

    /**
     * @var string
     */
    private $descricao;
}