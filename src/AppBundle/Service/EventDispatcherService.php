<?php
/**
 * Created by PhpStorm.
 * User: lab05usuario21
 * Date: 09/06/2018
 * Time: 14:30
 */

namespace AppBundle\Service;


use AppBundle\Event\InscricaoEvent;
use Domain\Model\Inscricao;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class EventDispatcherService
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * EventDispatcherService constructor.
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function dispatcheInscricaoEvent(Inscricao $inscricao)
    {
        $inscricaoEvent = new InscricaoEvent($inscricao);
        $this->eventDispatcher->dispatch(InscricaoEvent::INSCRICAO, $inscricaoEvent);
    }

}