<?php
/**
 * Created by PhpStorm.
 * User: vchastinet
 * Date: 05/06/18
 * Time: 21:55
 */

namespace AppBundle\Controller;

use Presentation\DataTransferObject\InscricaoDTO;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InscricaoController extends Controller
{
    /**
     * @Route("/inscricao/inscrever")
     * @param Request $request
     */
    public function inscreverAction(Request $request) {
        $serializerService = $this->get('infra.serializer.service');
        $inscricaoService = $this->get('app.inscricao.service');

        try {
            $inscricao = $serializerService->converter($request->getContent(), InscricaoDTO::class);
            $resultado = $inscricaoService->inscrever($inscricao);
            // dump($inscricao); die;
        } catch(\Exception $exception) {
            return new Response($exception->getMessage(), 400);
        }

        return new Response($resultado);
    }

}